import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Promise from 'bluebird'
import axios from 'axios'
import _ from 'lodash'
import styled from 'styled-components'
import MediaQuery from 'react-responsive'
import Router from './routes/'
import { preloadStaticImages, toggleMenu } from '@/actions/general'
import { 
  Preloader,
  Menu,
  Logo,
  Heading,
  Button,
  Content,
  Footer
} from '@/components/general'
import { Header, HeaderBox } from '@/components/layout'

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  background: #f7f9fa;
`

class App extends Component {

  constructor(props) {
    super(props)
  }
  
  componentWillMount() {
    this.props.preloadStaticImages()
  }

  render() {
    const { loading } = this.props.state.general
    if (loading) {
        return <Wrapper><Preloader /></Wrapper>
    } else {
      return (
        <Wrapper>
          <Header>
            <Menu/>
            <Logo light={true} />
            <HeaderBox>
              <Heading primary={false} type="heading">Pear Parfait with Bayleaf</Heading>
              <Content light={true}>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </Content>
              <Button>Read More</Button>
            </HeaderBox>  
          </Header>
          <Router data={this.props}/>
          <Footer/>
        </Wrapper>
      )
    }
  }

}

function mapStateToProps(state) {
  return {
    state: {
      general: state.general,
    }
  }
}

let DecoratedApp = App
DecoratedApp = connect(mapStateToProps, { preloadStaticImages, toggleMenu })(DecoratedApp)
DecoratedApp = withRouter(DecoratedApp)

export default DecoratedApp
