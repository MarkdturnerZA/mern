import styled from 'styled-components'
import { media } from '@/lib/media'

const Container = styled.div`
  position: relative;
  width: 100%;
	height: auto;
	margin-top: 15%;
	font-size: 1.3em;
	text-align: ${props => props.align};
`
const CopyBox = (props) => {
    return (
			<Container align={props.align} className="CopyBox">
        {props.children}
      </Container>
    )
}

export default CopyBox
