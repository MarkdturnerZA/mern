import styled from 'styled-components'

const ContainerFixed = styled.div`
  position: relative;
  width: 80%;
  height: 100%;
  margin: 0 auto;
`

const Container = (props) => {
  return (
    <ContainerFixed>
      {props.children}
    </ContainerFixed>
  )
}

export default Container
