import styled from 'styled-components'
import { media } from '@/lib/media'

const Container = styled.div`
  position: relative;
  width: 100%;
	height: auto;
	h1 {
		display: block;
    width: 80%;
		font-size: 3.5em;
	}
	p {
		padding: 4vh 0 6vh 0;
	}
	${media.tablet`
		width: 44vw;
		h1 {
			font-size: 6.5em;
		}
		p {
			padding: 4vh 0 6vh 0;
		}
  `}
	${media.desktopL`
		h1 {
			font-size: 7.5em;
		}
  `}
`

const HeaderBox = (props) => {
	return (
		<Container className="HeaderBox">
			{props.children}
		</Container>
	)
}

export default HeaderBox
