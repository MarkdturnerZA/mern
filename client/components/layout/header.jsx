import styled from 'styled-components'
import { ArrowDown } from '@/components/general'
import { media } from '@/lib/media'
import {
  backgroundHeaderDesktop,
  backgroundHeaderMobile
} from '@/lib/images'

const HeaderContainer = styled.div`
  position: relative;
  background-image: url(${backgroundHeaderMobile});
  background-size: cover;
  width: 100%;
  height: 100vh;
  z-index: 1;
  ${media.tablet`
    background-image: url(${backgroundHeaderDesktop});
  `}
`
const Wrapper = styled.div`
  display: flex;
  -ms-flex-align: center;
  -webkit-align-items: center;
  -webkit-box-align: center;
  align-items: center;
  position: relative;
  width: 80%;
  height: 100%;
  margin: 0 auto;
  img {
    position: absolute;
    top: 5%;
    left: 0;
  }
`

const Header = (props) => {
  return (
    <HeaderContainer>
      <Wrapper>
        {props.children}
        <ArrowDown />
      </Wrapper>
    </HeaderContainer>
  )
}

export default Header
