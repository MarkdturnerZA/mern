import styled from 'styled-components'
import { media } from '@/lib/media'

const Container = styled.div`
  position: relative;
  width: 100%;
	height: auto;
	h2 {
		font-size: 2.41em;
		margin-bottom: 1em;
	}
	${media.tablet`
		padding: 0 6.5vw;
	`}
`

const PageWrapper = (props) => {
	return (
		<Container className="PageWrapper">
				{props.children}
		</Container>
	)
}

export default PageWrapper
