import React, { Component } from 'react'
import styled, { css } from 'styled-components'
import { Logo } from '@/components/general'
import { Container } from '@/components/layout'
import { media } from '@/lib/media'

const BREAKPOINT_MOBILE = process.env.BREAKPOINT_MOBILE
const BREAKPOINT_TABLET = process.env.BREAKPOINT_TABLET

const Wrapper = styled.div`
  height: 200px;
  width: 100%;
  background: #ffffff;
`
const Info = styled.div`
  position: relative;
  height: 4.5vh;
`

const VerticalCenter = styled.div`
  height: 100%;
  width: 100%;
  background: #ffffff;
  display: inline-flex;
  justify-content: space-between;
  align-items: center;
  ${media.mobile`
    flex-wrap: wrap;
    text-align: center;
    img {
      margin: 0 auto;
    }
  `}
`

const Icon = styled.div`
  height: 30px;
  width: 30px;
  border-radius: 100%;
  background: #e8e9eb;
  position: relative;
  display: inline-block;
  margin: 0 0.35vw;
  a {
    position: absolute;
    left: 0;
    right: 0;
    text-align: center;
    top: 50%;
    transform: translateY(-50%);
    color: #ffffff;
  }
  &:hover {
    background: ${({ theme }) => theme.primary};
    transition: background 750ms ease-in-out;
  }
`
const ContactDetails = styled.div`
  display: inline-block;
  position: relative;
  top: -50%;
  transform: translateY(50%);
  ${media.mobile`
    width: 100%;
  `}
  a {
    text-decoration: none;
    color: ${({ theme }) => theme.primary};
    margin: 0 5vw 0 0;
    font-weight: bold;
  }
`

class Footer extends Component {
	componentWillMount() {

	}

	render() {

		return (
      <Wrapper>
			  <Container>
          <VerticalCenter>
            <Logo light={false} />
            <Info>
              <ContactDetails>
                <a href="tel:+27215555555">+27 21 555 5555</a>
                <a href="mailto:info@hellofood.com">info@hellofood.com</a>
              </ContactDetails>
              <Icon>
                <a href="#"><i className="fab fa-twitter" /></a>
              </Icon>
              <Icon>
                <a href="#"><i className="fab fa-facebook-square" /></a>
              </Icon>
              <Icon>
                <a href="#"><i className="fab fa-instagram" /></a>
              </Icon>
            </Info>
          </VerticalCenter>
			  </Container>
      </Wrapper>
		)
	}
}

export default Footer