import React, { Component } from 'react'
import styled, { css } from 'styled-components'
import { TextInput, Button} from '@/components/general'
import { media } from '@/lib/media'

const FormContainer = styled.div`
  form {
    width: 100%;
    margin: 0 auto;
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    margin-bottom: 8em;
    button {
      margin-top: 4em;
    }
    ${media.tablet`
		  width: 80%;
    `}
  }
`

class RecipeForm extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <FormContainer>
        <form>

          <TextInput
            name='title'
            label='Recipe Title'
            value={this.props.recipe.title || ''}
            onChange={this.props.onChange}
            full={false}
          />

          <TextInput
            name='recipeType'
            label='Recipe Type'
            value={this.props.recipe.recipeType || ''}
            onChange={this.props.onChange}
            full={false}
          />

          <TextInput
            name='recipeImage'
            label='Recipe Image'
            value={this.props.recipe.recipeImage || ''}
            onChange={this.props.onChange}
            full={true}
          />

          <TextInput
            name='ingredients'
            label='Recipe Ingredients'
            value={this.props.recipe.ingredients || ''}
            onChange={this.props.onChange} 
            full={true}
          />

          {/* <select>
            <option value="1">One</option>
            <option value="2">Two</option>
            <option value="3">Three</option>
            <option value="4">Four</option>
            <option value="5">Five</option>
          </select> */}

          {!this.props.isEditing && (
            <Button primary={true} disabled={this.props.saving} onClick={this.props.onSave}>
              {this.props.saving ? 'Saving...' : 'Save'}
            </Button>
          )}
       
          {this.props.isEditing && (
            <Button primary={true} disabled={this.props.saving} onClick={this.props.onUpdate}>
              {this.props.saving ? 'Saving...' : 'Update'}
            </Button>
          )}
    
          <Button primary={true} onClick={this.props.onCancel}>Cancel</Button> 
  
          


        </form>
      </FormContainer>
    )
  }
}

export default RecipeForm
