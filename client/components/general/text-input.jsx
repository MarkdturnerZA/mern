import React from 'react'
import styled, { css } from 'styled-components'
import { media } from '@/lib/media'


const FieldGroup = styled.div`
  margin: 2%;
  box-sizing: border-box;
  width: 100%;
  ${media.tablet`
		  width: ${props => props.full ? '100%' : '46%'};
  `}
  height: auto;
  label {
    font-size: 1.25em;
    margin-bottom: 0.25em;
    font-weight: bold;
    display: block;
  }
`

const Input = styled.input`
  display: block;
  width: 100%;
  height: 2.5em;
  border: solid;
  background: ${({ theme }) => theme.primary};
  color: ${({ theme }) => theme.secondary};
  font-size: 2em;
  padding-left: 10px;
  border-color: ${({ theme }) => theme.primary};
  &:focus {
    outline: none;
  }
`

const TextArea = styled.textarea`
  display: block;
  width: 100%;
  border: solid;
  font-size: 2em;
  padding-top: 10px;
  padding-left: 10px;
  background: ${({ theme }) => theme.primary};
  color: ${({ theme }) => theme.secondary};
  border-color: ${({ theme }) => theme.primary};
  min-height: 10em;
  &:focus {
    outline: none;
  }
`

const TextInput = ({ name, label, onChange, placeholder, value, error, type="text", full, input=true}) => {

  return (
    <FieldGroup full={full}>
      <label htmlFor={name}>{label}</label>
      <div className="field">
        {input && (
        <Input
          type={type}
          name={name}
          className="form-control"
          placeholder={placeholder}
          value={value}
          onChange={onChange}
        />
        )}
        {!input && (
        <TextArea
          type={type}
          name={name}
          className="form-control"
          placeholder={placeholder}
          value={value}
          onChange={onChange}/>
        )}
        {error && <div className="alert alert-danger">{error}</div>}
      </div>
    </FieldGroup>
  )
}

export default TextInput
