import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled, { css } from 'styled-components'
import { toggleMenu } from '@/actions/general'
import { media } from '@/lib/media'

const MenuB = styled.div`
  ${media.mobile`
    display: flex;
  `}
  display: none;
  position: absolute;
  z-index: 999;
  top: 5%;
  right: 0%;
  span {
    color: #fff;
    font-size: 1.5em;
    font-weight: bold;
    display: inline-block;
    padding-right: 10px;
  }
`

const Icon = styled.div`
  width: 30px;
  height: 20px;
  display: inline-block;
  transform: rotate(0deg);
  transition: top opacity width left transform .5s ease-in-out;
  cursor: pointer;
  span {
    background: #ffffff;
    display: block;
    position: absolute;
    height: 3px;
    width: 100%;
    opacity: 1;
    left: 0;
    transform: rotate(0deg);
    transition: .25s ease-in-out;
    &:nth-child(1) {
      top: 0%;
    }
    &:nth-child(2),
    &:nth-child(3) {
      top: 50%;
    }
    &:nth-child(4) {
      top: 100%;
    }
  }
  &.open {
    span {
      &:nth-child(1) {
        opacity: 0;
        top: 50%;
        width: 0%;
        left: 50%;
      }
      &:nth-child(2) {
        transform: rotate(45deg);
      }
      &:nth-child(3) {
        transform: rotate(-45deg);
      }
      &:nth-child(4) {
        opacity: 0;
        top: 50%;
        width: 0%;
        left: 50%;
      }
    }
  }
`

class MenuButton extends Component {
  constructor(props) {
    super(props)
    this.handleOnClick = this._handleOnClick.bind(this)
  }

  _handleOnClick() {
    this.props.toggleMenu()
  }

  render() {
    const { menu } = this.props.state.general

    return (
      <MenuB onClick={this.handleOnClick}>
        <span>Menu</span>
        <Icon className={menu.open ? 'open' : null}>
          <span></span>
          <span></span>
          <span></span>
          <span>
            <div></div>
          </span>
        </Icon>
      </MenuB>
    )
  }
}

function mapStateToProps(state) {
  return {
    state: {
      general: state.general,
    },
  }
}

let DecoratedMenuButton = MenuButton
DecoratedMenuButton = connect(mapStateToProps, { toggleMenu })(DecoratedMenuButton)

export default DecoratedMenuButton
