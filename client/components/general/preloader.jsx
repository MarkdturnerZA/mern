import React from 'react'
import styled, { keyframes } from 'styled-components'

const PeloaderContainer = styled.div`
  display: flex;
  -ms-flex-align: center;
  -webkit-align-items: center;
  -webkit-box-align: center;
  align-items: center;
  position: fixed;
  width: 100vw;
  height: 100vh;
  left: 0;
  top: 0;
  z-index: 1;
  background: ${({ theme }) => theme.primary};
`

const SkCircleFadeDelay = keyframes`
  100% { 
    opacity: 0; 
  }
  40% { 
    opacity: 1; 
  } 
`
const SkFadingCircle = styled.div`
  margin: 0 auto;
  width: 40px;
  height: 40px;
  position: relative;
  .sk-circle {
    width: 100%;
    height: 100%;
    position: absolute;
    left: 0;
    top: 0;
    :before {
      content: '';
      display: block;
      margin: 0 auto;
      width: 15%;
      height: 15%;
      background-color: ${({ theme }) => theme.secondary};
      border-radius: 100%;
      animation: ${SkCircleFadeDelay} 1.2s infinite ease-in-out both;
    }
  }
  .sk-circle2 {
    transform: rotate(30deg);
    :before {
      animation-delay: -1.1s; 
    }
  }
  .sk-circle3 {
    transform: rotate(60deg);
    :before {
      animation-delay: -1s;
    }
  }
  .sk-circle4 {
    transform: rotate(90deg);
    :before {
      animation-delay: -0.9s;
    }
  }
  .sk-circle5 {
    transform: rotate(120deg);
    :before {
      animation-delay: -0.8s;
    }
  }
  .sk-circle6 {
    transform: rotate(150deg);
    :before {
      animation-delay: -0.7s;
    }
  }
  .sk-circle7 {
    transform: rotate(180deg);
    :before {
      animation-delay: -0.6s;
    }
  }
  .sk-circle8 {
    transform: rotate(210deg);
    :before {
      animation-delay: -0.5s;
    }
  }
  .sk-circle9 {
    transform: rotate(240deg);
    :before {
      animation-delay: -0.4s;
    }
  }
  .sk-circle10 {
    transform: rotate(270deg);
    :before {
      animation-delay: -0.3s;
    }
  }
  .sk-circle11 {
    transform: rotate(300deg); 
    :before {
      animation-delay: -0.2s;
    }
  }
  .sk-circle12 {
    transform: rotate(330deg); 
    :before {
      animation-delay: -0.1s;
    }
  }
`

const Preloader = (props) => {
	return (
    <PeloaderContainer>
      <SkFadingCircle className="sk-fading-circle">
        <div className="sk-circle1 sk-circle"></div>
        <div className="sk-circle2 sk-circle"></div>
        <div className="sk-circle3 sk-circle"></div>
        <div className="sk-circle4 sk-circle"></div>
        <div className="sk-circle5 sk-circle"></div>
        <div className="sk-circle6 sk-circle"></div>
        <div className="sk-circle7 sk-circle"></div>
        <div className="sk-circle8 sk-circle"></div>
        <div className="sk-circle9 sk-circle"></div>
        <div className="sk-circle10 sk-circle"></div>
        <div className="sk-circle11 sk-circle"></div>
        <div className="sk-circle12 sk-circle"></div>
      </SkFadingCircle>
    </PeloaderContainer>
  )
}

export default Preloader