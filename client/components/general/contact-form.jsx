import React, { Component } from 'react'
import styled, { css } from 'styled-components'
import { TextInput, Button } from '@/components/general'
import { media } from '@/lib/media'
import _ from 'lodash'

const FormContainer = styled.div`
  form {
    width: 100%;
    margin: 0 auto;
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    margin-bottom: 8em;
    button {
      margin-top: 4em;
    }
    ${media.tablet`
		  width: 80%;
    `}
  }
`

const FieldGroup = styled.div`
  margin: 2%;
  box-sizing: border-box;
  width: 100%;
  ${media.tablet`
		  width: ${props => props.full ? '100%' : '46%'};
  `}
  height: auto;
  label {
    font-size: 1.25em;
    margin-bottom: 0.25em;
    font-weight: bold;
    display: block;
  }
`

const Error = styled.span`
  display: block;
  font-size: 1.125em;
  color: red;
  text-align: center;
  width: 100%;
`

class ContactForm extends Component {

  constructor() {
    super()
    this.state = {
      invalid: false,
      displayError: false,
      contactForm: {}
    }
    this.handleSubmit = this._handleSubmit.bind(this)
    this.updateContactState = this._updateContactState.bind(this)
    // this.handleHighlight = this.handleHighlight.bind(this)
  }

  _updateContactState(event) {
    const field = event.target.name
    const contactForm = this.state.contactForm
    contactForm[field] = event.target.value
    return this.setState({ contactForm: contactForm })
  }

  _handleSubmit(event) {
    event.preventDefault()
    if (!event.target.checkValidity()) {
      this.setState({
        invalid: true,
        displayError: true,
        inputError: event.target.name
      })
      return;
    }
    const form = event.target
    const data = new FormData(form)


    this.setState({
      invalid: false,
      displayErrors: false,
    })

  }

  _renderData () {
    const details = this.state.contactForm
    let data = _.map(details, (value, key) => {
      return (
        <pre>{key}: {value}</pre>
      )
    })
    return data
  }
  
  render() {
    const { details, invalid, displayError, contactForm } = this.state
    return (
      <FormContainer>
          <div>
            <span>Captured Data:</span>
            <pre>{this._renderData()}</pre>
          </div>
      <form onSubmit={this.handleSubmit} noValidate className={displayError ? 'displayErrors' : ''}>
        <TextInput
          name='name'
          label='First Name *'
          value={this.state.contactForm.name}
          onChange={this.updateContactState}
          full={false}
          required
        />

        <TextInput
          name='surname'
          label='Surname *'
          value={this.state.contactForm.surname}
          onChange={this.updateContactState}
          full={false}
          required
        />

        <TextInput
          name='email'
          label='Email *'
          value={this.state.contactForm.email}
          onChange={this.updateContactState}
          full={false}
          type='email'
          required
        />

        <TextInput
          name='mobile'
          label='Mobile'
          value={this.state.contactForm.mobile}
          onChange={this.updateContactState}
          full={false}
          type='tel'
        />

        <TextInput
          name='message'
          label='Message *'
          value={this.state.contactForm.message}
          onChange={this.updateContactState}
          full={true}
          type='text'
          input={false}
          required
        />
        
        <Button type="submit" primary={true}>
          Submit
        </Button>
      </form>
      </FormContainer>
    )
  }

}


export default ContactForm