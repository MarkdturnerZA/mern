import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled, { css } from 'styled-components'
import _ from 'lodash'
import { media } from '@/lib/media'

const Grid = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	margin-bottom: 8em;
`
const Column = styled.div`
	display: block;
`

const Card = styled.div`
	width: 150px;
	${media.tablet`
		width: 300px;
	`}
	-webkit-box-shadow: 11px 13px 8px -5px rgba(214,210,214,1);
	-moz-box-shadow: 11px 13px 8px -5px rgba(214,210,214,1);
	box-shadow: 11px 13px 8px -5px rgba(214,210,214,1);
	margin: ${props => (props.gutter * 2)}px ${props => props.gutter}px;
`
const Img = styled.img`
  display: block;
	width: 100%;
`

class Gallery extends Component {

	constructor(props) {
		super(props)
	}
	
	_grid = () => {
		let sum = this.props.images.length / 2
		let numberOfColumns = Math.round(sum)
		const columns = _.chunk(this.props.images, numberOfColumns)
		const results = columns.map((value, key) => {
			const item = value.map((value, key) => {
				return (
					<Card gutter={this.props.gutters} key={key}>
						<Img src={value} />
					</Card>
				)
			})
			return (
				<Column key={key}>
					{item}
				</Column>
			)
		})
		return results
	}
	render() {
		return (
			<Grid>
				{this._grid()}
			</Grid>
		)
	}
}


export default Gallery
