import React, { Component } from 'react'
import styled, { css } from 'styled-components'

const Title = styled.h1`
	font-family: ${({ theme }) => theme.primaryFont};
	font-size: 1em;
	color: ${props => props.primary ? '#c5b75b' : '#ffffff'};
`

const SubTitle = styled.h2`
	font-family:  ${({ theme }) => theme.primaryFont};
	font-size: 1em;
	color: ${props => props.primary ? '#c5b75b' : '#ffffff'};
`

const Heading = (props) => {
	switch (props.type) {
		case 'heading':
			return <Title primary={props.primary}>{props.children}</Title>
			break;
		case 'sub-heading':
			return <SubTitle primary={props.primary}>{props.children}</SubTitle>
			break;
		default:
			return <Title primary={props.primary}>{props.children}</Title>
	}
}

export default Heading