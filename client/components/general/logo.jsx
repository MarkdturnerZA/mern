import React, { Component } from 'react'
import styled, { css } from 'styled-components'
import {
	logoLight,
	logoDark
} from '@/lib/images'

const Img = styled.img`
  width: 100%;
  max-width: 170px;
`

const Logo = (props) => {
	if (props.light) {
		return <Img src={logoLight} />
	} else {
		return <Img src={logoDark} />
	}
}

export default Logo