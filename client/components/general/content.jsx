import React, { Component } from 'react'
import styled, { css } from 'styled-components'

const Copy = styled.p`
	font-family: 'Source Sans Pro', sans-serif;
	letter-spacing: 1px;
	font-size: 1.25em;
	line-height: 1.25;
	color: ${props => props.light ? '#ffffff' : '#000000'};
`

const Content = (props) => {
	return <Copy light={props.light}>{props.children}</Copy>
}

export default Content