import React, { Component } from 'react'
import styled, { css } from 'styled-components'


const Box = styled.div`
	position: absolute;
	margin: 0 auto;
	left: 0;
	right: 0;
	bottom: 0px;
	width: 15vw;
	height: 15vw;
`
const Triangle = styled.div`
	position: absolute;
	bottom: -1px;
	width: 0;
	height: 0;
	border-left: 7vw solid transparent;
	border-bottom: 6vw solid #f7f9fa;
	border-right: 7vw solid transparent;
	i {
		position: absolute;
    font-size: 4vw;
    right: 0;
    text-align: center;
    transform: translateX(50%);
    color: #000000;
    top: 1vw;
	}
`

const ArrowDown = (props) => {
	return (
		<Box>
			<Triangle>
				<i className="fas fa-angle-down"></i>
			</Triangle>
		</Box>
	)
}

export default ArrowDown
