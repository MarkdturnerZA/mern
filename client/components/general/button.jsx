import React, { Component } from 'react'
import styled, { css } from 'styled-components'

const CtaButton = styled.button`
	font-family: 'Source Sans Pro',sans-serif;
	font-weight: bold;
	font-size: 1em;
	color: ${props => props.primary ? props.theme.primary : '#ffffff'};
	border: solid 2px ${props => props.primary ? props.theme.primary : '#ffffff'};
	padding: 18px 0px;
	background: transparent;
	width: 100%;
	max-width: 240px;
	letter-spacing: 1px;
	display: inline-block;
	vertical-align: middle;
	-webkit-transform: perspective(1px) translateZ(0);
	transform: perspective(1px) translateZ(0);
	box-shadow: 0 0 1px rgba(0, 0, 0, 0);
	position: relative;
	-webkit-transition-property: color;
	transition-duration: 0.45s;
	margin: 0 2.5%;
	&:before {
		content: "";
		position: absolute;
		z-index: -1;
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		background: ${props => props.primary ? props.theme.primary : '#c5b75b'};
		-webkit-transform: scaleX(0);
		transform: scaleX(0);
		-webkit-transform-origin: 50%;
		transform-origin: 50%;
		-webkit-transition-property: transform;
		transition-property: transform;
		-webkit-transition-duration: 0.45s;
		transition-duration: 0.45s;
		-webkit-transition-timing-function: ease-out;
		transition-timing-function: ease-out;
	}
	&:hover {
		border-color: ${props => props.primary ? props.theme.primary : '#c5b75b'};
		color: #ffffff;
		cursor: pointer;
		&:before {
			-webkit - transform: scaleX(1);
			transform: scaleX(1);
		}
	}
	&:focus {
		border-color: ${props => props.primary ? props.theme.primary : '#c5b75b'};
		outline: none;
		color: #ffffff;
		&:before {
			-webkit - transform: scaleX(1);
			transform: scaleX(1);
		}
	}
	&:active {
		color: #ffffff;
		border-color: ${props => props.primary ? props.theme.primary : '#c5b75b'};
		&:before {
		-webkit - transform: scaleX(1);
			transform: scaleX(1);
		}
	}

`

const Button = (props) => {
	return <CtaButton primary={props.primary} onClick={props.onClick} style={props.styles}>{props.children}</CtaButton>
}

export default Button