import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom"
import { MenuButton } from '@/components/general'
import styled, { css } from 'styled-components'
import { media } from '@/lib/media'
import { toggleMenu } from '@/actions/general'

const MenuContainer = styled.div`

`

const ListContainer = styled.div`
	${media.mobile`
		display: flex;
		align-content: center;
		justify-content: center;
		position: fixed;
		top: 0;
		right: 0;
		width: 100vw;
		height: 100vh;
		background: ${props => props.theme.primary};
		height: 100%;
		z-index: 2;
		transition: transform 500ms ease-in-out;
		transform: translateX(101%);
		&.open {
			transform: translateX(0%);
		}
	`}
`

const List = styled.ul`
	display: flex;
	li {
		a {
			text-decoration: none;
			color: #ffffff;
			font-weight: bold;
			font-size: 1.25em;
			letter-spacing: 1px;
			margin-left: 2vw;
			cusror: pointer;
			transition: all 650ms ease-in-out;
		}
	}
	${media.mobile`
		height: auto;
    align-content: center;
		flex-wrap: wrap;
		li {
			width: 100%;
			text-align: center;
			font-size: 1.75em;
			line-height: 1.8;
			&:hover {
				a {
					color: ${props => props.theme.secondary}
				}
			}
		}
	`}
	${media.tablet`
		display: flex;
		position: absolute;
		top: 5%;
		right: 0;
	`}
`

class Menu extends Component {

	constructor (props) {
		super()
		this.handleOnClick = this._handleOnClick.bind(this)
	}

	_handleOnClick(){
		this.props.toggleMenu()
	}

	_renderMenuList() {
		const { toggleMenu } = this.props
		const menuList = this.props.state.menu.map((item) => {
			return <li key={item.title}><Link to={item.slug} onClick={this.handleOnClick}>{item.title}</Link></li>
		})
		return menuList
	}

	render() {
		const { menu } = this.props.state.general
		return (
			<MenuContainer>
				<MenuButton />
				<ListContainer className={menu.open ? 'open' : 'closed'}>
					<List>{this._renderMenuList()}</List>
				</ListContainer>
			</MenuContainer>
		)
	}
}

function mapStateToProps(state) {
	return {
		state: {
			menu: state.menu,
			general: state.general
		}
	}
}

let DecoratedMenu = Menu
DecoratedMenu = connect(mapStateToProps, { toggleMenu })(DecoratedMenu)
DecoratedMenu = withRouter(DecoratedMenu)

export default DecoratedMenu
