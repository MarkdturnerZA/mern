import React, { Component } from 'react'
import { Switch, Route, withRouter } from 'react-router-dom'

import Landing from '@/pages/landing'
import Recipes from '@/pages/recipes'
import Contact from '@/pages/contact'

class App extends Component {

  render() {
    return (
      <Switch>
        <Route exact path='/' component={Landing} />
        <Route exact path='/recipes' component={Recipes} />
        <Route exact path='/contact' component={Contact} />
      </Switch>
    )
  }

}

let DecoratedApp = App
DecoratedApp = withRouter(DecoratedApp)

export default DecoratedApp
