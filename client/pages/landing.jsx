import React, { Component } from 'react'
import styled, { css } from 'styled-components'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { Container, PageWrapper, CopyBox } from '@/components/layout'
import { Heading, Content, Gallery } from '@/components/general'
import _ from 'lodash'
import { media } from '@/lib/media'

class Landing extends Component {
  componentWillMount() {

  }

  render() {
    const { gallery } = this.props.state.general
    return (
      <Container>
        <PageWrapper>
          <CopyBox align="center">
            <Heading type="sub-heading" primary={true}>
              The Recipe
            </Heading>
            <Content>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium 
            </Content>
          </CopyBox>
          <CopyBox align="center">
            <Heading type="sub-heading" primary={true}>
              Gallery
            </Heading>
          </CopyBox>
          <Gallery images={gallery} gutters={10}></Gallery>
        </PageWrapper>
      </Container>
    )
  }
}

function mapStateToProps(state) {
  return {
    state: {
      general: state.general
    }
  }
}


let DecoratedLanding = Landing
DecoratedLanding = connect(mapStateToProps)(DecoratedLanding)
DecoratedLanding = withRouter(DecoratedLanding)

export default DecoratedLanding
