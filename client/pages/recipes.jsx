import React, { Component } from 'react'
import styled, { css } from 'styled-components'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Container } from '@/components/layout'
import { withRouter } from 'react-router-dom'
import { RecipeForm, Preloader, Heading, Button } from '@/components/general'
import { PageWrapper, CopyBox } from '@/components/layout'
import { media } from '@/lib/media'
import StarRatings from 'react-star-ratings'
import * as actionCreators from '@/actions/api'


const RecipesConatiner = styled.div`
  display: block;
  width: 100%;
`

const Recipe = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
  width: 90%;
  margin: 0 auto;
  margin-bottom: 3.5em;
  border-bottom: dotted 1px rgba(0, 0, 0, 0.5);
  padding-bottom: 1em;
  .col {
    display: inline-block;
    width: 100%;
    ${media.tablet`
      width: 50%;
      &.space-right {
        padding-right: 1em;
      }
	  `}
    h2 {
      font-size: 2em;
      margin: 1em 0em 0.25em;
      ${media.tablet`
        margin: 0em 0em 0.5em;
	  `}
    }
    h3 {
      font-size: 1.25em;
      margin: 1em 0em 1em;
      span {
        font-weight: bold;
      }
    }
    img {
      width: 100%;
    }
    button {
      width: 100%;
      margin: 2.5% 0;
      max-width: 100%;
      ${media.tablet`
        margin: 3.5% 2.5% 0 0;
        width: 45%;
	    `}
    }
  }
`
const customStyle = {
  margin: '0em 0em 2em 3em'
}

class Recipes extends Component {

  constructor(props) {
    super(props)
    this.state = {
      saving: false,
      isEditing: false,
      isCreating: false,
      loading: true,
      recipe: []
    }
    this.handleSaveRecipe = this._saveRecipe.bind(this);
    this.handleRecipeUpdate = this._updateRecipe.bind(this)
    this.updateRecipeState = this._updateRecipeState.bind(this)
    this.toggleCreateRecipe = this._toggleCreateRecipe.bind(this)
    this.toggleEdit = this._toggleEdit.bind(this)
    this.deleteRecipe = this._deleteRecipe.bind(this)
    this.cancelForm = this._cancelForm.bind(this)
  }
  
  componentWillMount() {
    this.props.actions.fetchRecipes()
  }


  _cancelForm() {
    this.setState({
      isEditing: false,
      isCreating: false,
      saving: false,
      recipe: []
    })
  }
  
  _toggleCreateRecipe() {
    this.setState({
      isEditing: false,
      isCreating: true,
      recipe: []
    })
  }
  
  _saveRecipe (e) {
    e.preventDefault()
    const recipe = this.state.recipe
    this.props.actions.createRecipe(recipe)
    this.setState({
      saving: true,
      isCreating: false
    })
  }

  _updateRecipeState(event) {
    const field = event.target.name
    const recipe = this.state.recipe
    recipe[field] = event.target.value
    return this.setState({ recipe: recipe })
  }

  _updateRecipe (id, e) {
    e.preventDefault()
    const recipe = this.state.recipe
    this.props.actions.updateRecipe(id, recipe)
    this.setState({
      saving: true,
      isEditing: false
    })
  }

  _toggleEdit(id) {
    const { recipes } = this.props.state
    let selectedRecipe = _.find(recipes.recipes, {_id: id})
    this.setState({ 
      saving: false,
      isEditing: true,
      isCreating: false,
      recipe: selectedRecipe
    })
  }

  _deleteRecipe (id) {
    this.props.actions.deleteRecipe(id)
  }

  _renderRecipes() {
    const { recipes } = this.props.state.recipes
    console.log(recipes)
    const render = _.map(recipes, (recipe, key) => {
      return (
        <Recipe key={recipe._id}>
          <div className="col space-right ">
            <img src={recipe.recipeImage} alt={recipe.title} />
          </div>
          <div className="col">
            <Heading type="sub-heading" primary={true}>
              {recipe.title}
            </Heading>
            <h3><span>Recipe Type:</span> {recipe.recipeType}</h3>
            <StarRatings
              rating={recipe.rating}
              starRatedColor="#1f2c3c"
              numberOfStars={5}
              starDimension="30px"
              starHoverColor="#c6b75a"
            />
            <Button primary={true} onClick={this.deleteRecipe.bind(this, recipe._id)}>
              Delete
            </Button>
            <Button primary={true} onClick={this.toggleEdit.bind(this, recipe._id)}>
              Edit
            </Button>
          </div>
        </Recipe>
      )
    })
    return render
  }

  render() {
    const { recipes } = this.props.state
    const { recipe } = this.state

    if (!recipes.loaded) {
      return (
        <Preloader/>
      )
    } else if (recipes.loaded && this.state.isEditing || recipes.loaded && this.state.isCreating) {
      return (
        <Container>
          <PageWrapper>
            <CopyBox align="center">
              <Heading type="sub-heading" primary={true}>
                {this.state.isEditing ? (recipe.title) : ('Add New Recipe') }
              </Heading>
            </CopyBox>
            <RecipeForm
              recipe={recipe}
              onUpdate={(e) => this.handleRecipeUpdate(recipe._id, e)}
              onSave={(e) => this.handleSaveRecipe(e)}
              onCancel={this.cancelForm}
              isEditing={this.state.isEditing}
              onChange={this.updateRecipeState}
              saving={this.state.saving}
            />
          </PageWrapper>
        </Container>
      )
    } else { 
      return (
        <Container>
          <PageWrapper>
            <CopyBox align="center">
              <Heading type="sub-heading" primary={true}>
                Recipes
              </Heading>
            </CopyBox>
            <Button primary={true} onClick={this.toggleCreateRecipe} styles={customStyle}>
              Add Recipe
            </Button>
            <RecipesConatiner>
              {this._renderRecipes()}
            </RecipesConatiner>
          </PageWrapper>
        </Container>
      )
    }
  }
}

function mapStateToProps(state) {
  return {
    state: {
      general: state.general,
      recipes: state.api
    }
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  }
}


let DecoratedRecipes = Recipes
DecoratedRecipes = connect(mapStateToProps, mapDispatchToProps)(DecoratedRecipes)
DecoratedRecipes = withRouter(DecoratedRecipes)

export default DecoratedRecipes