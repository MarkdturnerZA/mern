import React, { Component } from 'react'
import styled, { css } from 'styled-components'
import { withRouter } from 'react-router-dom'
import { ContactForm, Heading } from '@/components/general'
import { Container, PageWrapper, CopyBox } from '@/components/layout'


class Contact extends Component {
  componentWillMount() {

  }

  render() {
    return (
      <Container>
        <PageWrapper>
          <CopyBox align="center">
            <Heading type="sub-heading" primary={true}>
              Contact Us
            </Heading>
          </CopyBox>
          <ContactForm />
        </PageWrapper>
      </Container>
    )
  }
}


export default withRouter(Contact)
