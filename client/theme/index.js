const theme = {
  primary: '#1f2c3c',
  secondary: '#c2b851',
  primaryFont: `'Rakkas', cursive`,
  secondaryFont: `'Source Sans Pro', sans-serif`
}

export default theme
