// THIS IS DUMMY DATA
export default function () {
  return [{
      id: 1,
      title: 'Intro',
      slug: '/'
    },
    {
			id: 2,
      title: 'Recipes',
      slug: '/recipes'
    },
		{
			id: 3,
			title: 'Gallery',
			slug: '/gallery'
		},
		{
			id: 4,
			title: 'Contact',
			slug: '/contact'
		}
  ]
}

