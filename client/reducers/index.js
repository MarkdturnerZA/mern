import { combineReducers } from 'redux'
import general from '@/reducers/general'
import api from '@/reducers/api'
import menu from '@/reducers/menuStatic'


const rootReducer = combineReducers({
  api,
  general,
  menu
})

export default rootReducer
