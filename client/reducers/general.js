import {
  TOGGLE_MENU,
  PRELOAD_STATIC_IMAGES,
  GALLERY_IMAGES_LOADED,
  GALLERY_IMAGES_ERROR
} from '@/actions/general'

const initialSate = {
  loading: true,
  menu: {
    open: false
  }
}

function general(state = initialSate, action) {
  
  switch (action.type) {
         
    case TOGGLE_MENU:
      return {
        ...state,
        menu: {
          ...state.menu,
          open: !state.menu.open
        }
      }
      break

    case GALLERY_IMAGES_LOADED: {
      return {
        ...state,
        gallery: action.data,
        loading: false,
        error: false,
      }
    }

    case GALLERY_IMAGES_ERROR: {
      return {
        ...state,
        loading: true,
        error: true,
      }
    }

    default:
      return state
  }

}

export default general
