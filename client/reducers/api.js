import {
  RECIPES_LOADED,
  CREATED_RECIPE_SUCCESS,
  UPDATE_RECIPE_SUCCESS,
  DELETE_RECIPE_SUCCESS,
  RECIPES_ERROR
} from '@/actions/api'
import _ from 'lodash'
const initialSate = {
  recipes: null,
  loaded: false,
  error: false,
}

function apiCall(state = initialSate, action) {
  switch (action.type) {
    case RECIPES_LOADED:
    console.log(action.data)
      {
        return {
          ...state,
          recipes: action.data,
          loaded: true,
          error: false,
        }
      }

    case RECIPES_ERROR:
      {
        return {
          ...state,
          loaded: false,
          error: true,
        }
      }
     
    case CREATED_RECIPE_SUCCESS:
      return {
        ...state,
        recipes: [
          ...state.recipes, action.data
        ],
        loaded: true
      }

    case UPDATE_RECIPE_SUCCESS:
      {
        const newState = Object.assign([], state)
        return newState
      }
      
    case DELETE_RECIPE_SUCCESS:
      {
        const newState = Object.assign([], state)
        const updatedState = _.remove(newState.recipes, function (recipe) {
          return recipe._id == action.data
        })
        return newState
      }

    default:
      return state
  }

}

export default apiCall

