import gelleryImage01 from '@/static/images/gallery/image-one.jpg'
import gelleryImage02 from '@/static/images/gallery/image-two.jpg'
import gelleryImage03 from '@/static/images/gallery/image-three.jpg'
import gelleryImage04 from '@/static/images/gallery/image-four.jpg'
import gelleryImage05 from '@/static/images/gallery/image-five.jpg'

export {
  gelleryImage01,
  gelleryImage02,
  gelleryImage03,
  gelleryImage04,
  gelleryImage05
}

