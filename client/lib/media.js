import styled, { css } from 'styled-components'
import _ from 'lodash'

export const media = _.reduce(MEDIA, function(accumulator, value) {
  accumulator[value.name] = (...args) => css`
    @media (${value.condition}: ${value.size}px) {
      ${css(...args)}
    }
  `
  return accumulator
}, {})
