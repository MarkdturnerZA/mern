import logoLight from '@/static/images/logo-light.png'
import logoDark from '@/static/images/logo-dark.png'
import backgroundHeaderDesktop from '@/static/images/header-desktop.jpg'
import backgroundHeaderMobile from '@/static/images/header-mobile.jpg'

export {
	logoLight,
	logoDark,
	backgroundHeaderDesktop,
	backgroundHeaderMobile
}
