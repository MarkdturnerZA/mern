import axios from 'axios'

var axiosInstance = axios.create({
  baseURL: 'http://localhost:4001',
})

export default axiosInstance
