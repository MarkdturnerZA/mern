import axios from '@/lib/request'
import Promise from 'bluebird'

export const RECIPES_LOADED = 'RECIPES_LOADED'
export const CREATED_RECIPE_SUCCESS = 'CREATED_RECIPE_SUCCESS'
export const UPDATE_RECIPE_SUCCESS = 'UPDATE_RECIPE_SUCCESS'
export const DELETE_RECIPE_SUCCESS = 'DELETE_RECIPE_SUCCESS'
export const RECIPES_ERROR = 'RECIPES_ERROR'

export function recipesLoaded(data) {
  return {
    type: RECIPES_LOADED,
    data
  }
}

export function createRecipeSuccess(data) {
  return {
    type: CREATED_RECIPE_SUCCESS,
    data
  }
}


export function recipeUpdateSuccess(data) {
  return {
    type: UPDATE_RECIPE_SUCCESS,
    data
  }
}

export function recipeDeleteSuccess(data) {
  return {
    type: DELETE_RECIPE_SUCCESS,
    data
  }
}

export function recipesError(data) {
  return {
    type: RECIPES_ERROR,
    data
  }
}


export function fetchRecipes() {
  return dispatch => {
    axios.get('/api/recipes')
      .then(response => {
        dispatch(recipesLoaded(response.data))
      })
      .catch(error => {
        dispatch(recipesError(error))
      })
  }
}

export function createRecipe(data) {
  return dispatch => {
  axios.post('/api/recipe/', {
      title: data.title,
      recipeType: data.recipeType,
      ingredients: data.ingredients,
      method: data.method,
      rating: data.rating || 3,
      recipeImage: data.recipeImage || 'https://loremflickr.com/g/600/400/food'
    })
    .then(response => {
      dispatch(createRecipeSuccess(response.data))
    })
    .catch(error => {
      dispatch(recipesError(error))
    })
  }
}

export function updateRecipe(id, data) {
  return dispatch => {
    axios.put(`/api/recipe/${id}`, {
      data
    })
    .then(response => {
      dispatch(recipeUpdateSuccess(response.data.data))
    })
    .catch(error => {
      dispatch(recipesError(error))
    })
  }
}


export function deleteRecipe(id) {
  return dispatch => {
    axios.delete(`/api/recipe/${id}`)
      .then(response => {
        dispatch(recipeDeleteSuccess(response.data._id))
      })
      .catch(error => {
        dispatch(recipesError(error))
      })
  }
}



