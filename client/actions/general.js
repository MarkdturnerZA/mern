import axios from '@/lib/request'
import Promise from 'bluebird'

import * as Images from '@/lib/gallery'

export const TOGGLE_MENU = 'TOGGLE_MENU'
export const PRELOAD_STATIC_IMAGES = 'PRELOAD_STATIC_IMAGES'
export const GALLERY_IMAGES_LOADED = 'GALLERY_IMAGES_LOADED'
export const GALLERY_IMAGES_ERROR = 'GALLERY_IMAGES_ERROR'
export const FETCH_IMAGES = 'FETCH_IMAGES'

export function toggleMenu(menuState) {
  return {
    type: TOGGLE_MENU,
    menuState,
  }
}

export function imagesLoaded(data) {
  return {
    type: GALLERY_IMAGES_LOADED,
    data
  }
}

export function imagesError(data) {
  return {
    type: GALLERY_IMAGES_ERROR,
    data
  }
}

export function preloadStaticImages() {
  const ImagesPromises = _.map(Images, (value, key) => {
    return new Promise((resolve, reject) => {
      const img = new Image()
      img.src = value
      img.onload = () => {
        resolve(value)
      }
    })
  })
  return dispatch => {
    Promise.all(ImagesPromises)
    .then(result => {
      dispatch(imagesLoaded(result))
    })
    .catch(error => {
      dispatch(imagesError(error))
    })
  }
}



