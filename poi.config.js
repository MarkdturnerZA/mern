var path = require('path')
module.exports = (options, req) => ({
  entry: './client/index.js',
  define: {
    BREAKPOINT_TABLET: 1024,
    BREAKPOINT_MOBILE: 768,
    MEDIA: [
      { name: "'mobile'", size: 767, condition: "'max-width'" },
      { name: "'tablet'", size: 768, condition: "'min-width'" },
      { name: "'desktopS'", size: 1024, condition: "'min-width'" },
      { name: "'desktopM'", size: 1366, condition: "'min-width'" },
      { name: "'desktopL'", size: 1599, condition: "'min-width'" },
      { name: "'heightQuery'", size: 600, condition: "'min-height'"}
    ],
    API_BASE_URL: "'http://localhost:4001/api/'",
  },
  dist: '/test/',
  publicPath: '/assets/',
  html: {
    filename: 'index.html',
    APP_NAME: 'Hello Food'
  },
  configureWebpack(config) {
    config.resolve.alias = {
      '@': path.resolve(__dirname) + '/client'
    }
    return config
  }
})
