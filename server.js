const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors')

const app = express();
const router = express.Router();

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))

// Replace with your mongoLab URI
const MONGO_URI = 'mongodb://admin:password123@ds247410.mlab.com:47410/hellofood';
if (!MONGO_URI) {
  throw new Error('You must provide a MongoLab URI');
}

mongoose.Promise = Promise;

mongoose.connect(MONGO_URI);
mongoose.connection
  .once('open', () => console.log('Connected to MongoLab instance.'))
  .on('error', error => console.log('Error connecting to MongoLab:', error));


// mongoose.set('debug', true)

router.get('/', (req, res) => {
  res.json({
    "message": "API Initialized!"
  });
});

app.use('/api', router);
require('./server/routes/routes.js')(app);

// listen for requests
app.listen(4001, () => {
  console.log('Api running on port 4001');
});

