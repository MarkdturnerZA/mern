module.exports = (app) => {
    const recipe = require('../controllers/controller.js');
    app.post('/api/recipe', recipe.create);
    app.get('/api/recipes', recipe.findAll);
    app.get('/api/recipe/:recipeId', recipe.findOne);
    app.put('/api/recipe/:recipeId', recipe.update);
    app.delete('/api/recipe/:recipeId', recipe.delete);
}