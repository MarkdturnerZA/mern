const Recipe = require('../models/model.js');

function isEmpty(obj) {
	for (var key in obj) {
		if (obj.hasOwnProperty(key))
			return false
	}
	return true
}

exports.create = (request, response) => {
	if (isEmpty(request.body)) {
		return response.status(400).send({
			success: false,
			message: 'Recipe content can not be empty'
		})
	}

	const recipe = new Recipe({
		title: request.body.title || 'Untitled recipe',
		recipeType: request.body.recipeType,
		ingredients: request.body.ingredients,
		method: request.body.method,
		rating: request.body.rating || 3,
		recipeImage: request.body.recipeImage || 'https://loremflickr.com/g/600/400/food'
	})

	recipe.save()
	.then(data => {
		response.send(data)
	}).catch(err => {
		response.status(500).send({
			message: err.message || 'Some error occurred while creating the recipe.'
		})
	})
}


exports.findAll = (request, response) => {
	Recipe.find()
	.then(data => {
		response.send(data)
	}).catch(err => {
		response.status(500).send({
			message: err.message || 'Some error occurred while retrieving recipes.'
		})
	})
}



exports.findOne = (request, response) => {
	Recipe.findById(req.params.recipeId)
	.then(data => {
			if (isEmpty(request.body)) {
				return response.status(404).send({
					success: false,
					message: 'Note not found with id ' + req.params.recipeId
				});            
			}
			response.send(data)
	}).catch(err => {
			if(err.kind === 'ObjectId') {
				return response.status(404).send({
					success: false,
					message: 'Note not found with id ' + req.params.recipeId
				});                
			}
			return response.status(500).send({
				message: 'Error retrieving note with id ' + req.params.recipeId
			})
	});
};

exports.update = (request, response) => {
	if (isEmpty(request.body)) {
		return response.status(400).send({
			message: 'Recipe content can not be empty'
		})
	}
	console.log(request.body.data)
	Recipe.findByIdAndUpdate(request.params.recipeId, {
		title: request.body.data.title || 'Untitled recipe',
		recipeType: request.body.data.recipeType,
		ingredients: request.body.data.ingredients,
		method: request.body.data.method,
		rating: 5,
		recipeImage: request.body.data.recipeImage || 'https://loremflickr.com/g/400/500/food'
	}, {new: true})
	.then(data => {
			if (isEmpty(request.body)) {
				return response.status(404).send({
					success: false,
					message: 'Recipe not found with id ' + request.params.recipeId
				});
			}
			response.send(data);
	}).catch(err => {
			if(err.kind === 'ObjectId') {
				return response.status(404).send({
					message: 'Recipe not found with id ' + request.params.recipeId
				});                
			}
			return response.status(500).send({
				message: 'Error updating recipe with id ' + request.params.recipeId
			});
	});
};

exports.delete = (request, response) => {
	Recipe.findByIdAndRemove(request.params.recipeId)
	// console.log(request.params.recipeId)
	// console.log(data)
	// return
	.then(recipe => {
			if (!recipe) {
				return response.status(404).send({
					message: 'Recipe not found with id ' + request.params.recipeId
				});
			}
			response.send({_id: request.params.recipeId, message: 'Recipe deleted successfully!'});
	}).catch(err => {
			if(err.kind === 'ObjectId' || err.name === 'NotFound') {
				return response.status(404).send({
					message: 'Recipe not found with id ' + request.params.recipeId
				});                
			}
			return response.status(500).send({
				message: 'Could not delete Recipe with id ' + request.params.recipeId
			});
	});
}
