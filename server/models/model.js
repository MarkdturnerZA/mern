const mongoose = require('mongoose');

const RecipeSchema = mongoose.Schema({
    title: String,
    recipeType: String,
    recipeImage: String,
    ingredients: Array,
    method: Array,
    rating: Number,
    created: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Recipe', RecipeSchema);