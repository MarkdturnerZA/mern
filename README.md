# React Template

React playground.

## Setting Up

```
$ npm install && npm run start || yarn && yarn start
```

## Still To Do
Creat Image uploader to load onto mLab.
ServerSide Rendering, with [Next](https://nextjs.org/).

## Built With

* [Poi](https://poi.js.org/)
* [React](https://reactjs.org/)
* [Redux](https://maven.apache.org/)
* [Axios](https://github.com/axios/axios)
* [Styled Components](https://www.styled-components.com/)
* [Express.Js](https://expressjs.com/)